FROM 192.168.23.113:60080/alaudaci/u14java:oraclejdk8

CMD ["mkdir", "/app"]
ADD demo-0.0.1.jar /app/
CMD ["java", "-Xmx200m", "-jar", "/app/demo-0.0.1.jar"]

EXPOSE 8899